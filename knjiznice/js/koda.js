
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

function mainGenerator(){
    var generiraniEHRidji =[];
    
    generirajPodatke(1,function(ID){
        alert("ID pacienta st1: "+ID);
        addInfo(ID,"1977-03-10T09:08",191,70);
        addInfo(ID,"1987-03-10T09:08",185,75);
        addInfo(ID,"1997-03-10T09:08",180,73);
        addInfo(ID,"2007-03-10T09:08",180,85);
        addInfo(ID,"2017-03-10T09:08",175,70);
        addInfo(ID,"2017-03-10T09:08",175,70);
    });
    
    generirajPodatke(2,function(ID){
        alert("ID pacienta st2: "+ID);
        addInfo(ID,"1967-03-10T09:08",170,80);
        addInfo(ID,"1977-03-10T09:08",175,85);
        addInfo(ID,"1987-03-10T09:08",180,75);
        addInfo(ID,"1997-03-10T09:08",178,70);
        addInfo(ID,"2007-03-10T09:08",175,83);
        addInfo(ID,"2017-03-10T09:08",170,70);
    })
    
    generirajPodatke(3,function(ID){
        alert("ID pacienta st3: "+ID);
        addInfo(ID,"1967-03-10T09:08",170,85);
        addInfo(ID,"1977-03-10T09:08",175,83);
        addInfo(ID,"1987-03-10T09:08",179,85);
        addInfo(ID,"1997-03-10T09:08",181,87);
        addInfo(ID,"2007-03-10T09:08",182,60);
        addInfo(ID,"2017-03-10T09:08",180,80);
    });
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta,callback) {
      sessionId = getSessionId();
      ehrId = "";
      var ime = "Ime"+stPacienta;
      var priimek = "Priimek"+stPacienta;
      var datumRojstva="1957-03-10T09:08"
  $.ajaxSetup({
         headers: {"EHR-Session": sessionId}
  });
           $.ajax({
              url: baseUrl+"/ehr",
              type: "POST",
              success: function(data) {
                  var ehrId = data.ehrId;
                  var partyData = {
                    firstNames: ime,
                    lastNames: priimek,
                    dateOfBirth: datumRojstva,
                    partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
                  };
                  $.ajax({
                     url: baseUrl+"/demographics/party",
                     type: "POST",
                     contentType: 'application/json',
                     data: JSON.stringify(partyData),
                     success: function(party) {
                         if(party.action == 'CREATE'){
                          callback(ehrId);
                         }
                     },
                    error: function(err){}
                  });
              }
           });

}

function addInfo(ehrid, datumvnosa, visina,teza){
    sessionId = getSessionId();
    $.ajaxSetup({
        headers: {"Ehr-Session": sessionId} 
    });
          
          var podatki = {
               "ctx/language": "en",
               "ctx/territory": "SI",
               "ctx/time": datumvnosa,
               "vital_signs/height_length/any_event/body_height_length": visina,
               "vital_signs/body_weight/any_event/body_weight": teza
       };
         
         var parametriZahteve = {
           ehrId: ehrid,
           templateId: 'Vital Signs',
           format: 'FLAT'
         };
         
         $.ajax({
            url: baseUrl + "/composition?" + $.param(parametriZahteve),
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(podatki), 
            
            success: function (res) {
               
            },
            error: function(err) { }
             
         });
}

function readInput(){
    
   sessionId = getSessionId();
   
   var ime = $("#ime").val();
   var priimek = $("#priimek").val();
   var datumRojstva = $("#datumRojstva").val();
   
   if(!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0){
          $("#message").html("<h1><span class='label " +
            "label-warning fade-in lb-lg'>Preverite vnesene podatke!</span></h1>");
       }else{
           $.ajaxSetup({
               headers: {"EHR-Session": sessionId}
           });
           $.ajax({
              url: baseUrl+"/ehr",
              type: "POST",
              success: function(data) {
                  var ehrId = data.ehrId;
                  var partyData = {
                    firstNames: ime,
                    lastNames: priimek,
                    dateOfBirth: datumRojstva,
                    partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
                  };
                  $.ajax({
                     url: baseUrl+"/demographics/party",
                     type: "POST",
                     contentType: 'application/json',
                     data: JSON.stringify(partyData),
                     success: function(party) {
                         if(party.action == 'CREATE'){
                               $("#message").html("<h1><span class='label " +
                              "label-success fade-in lb-lg'>EHR je bil uspesno kreiran: '"+
                              ehrId+"'</span></h1>");
                         }
                     },
                     error: function(err){
                         $("#message").html("<span class='label " +
                                 "label-danger fade-in'>Prišlo je do napake: '" +
                                JSON.parse(err.responseText).userMessage + "'!");
                     }
                  });
              }
           });
       }
   
}

function sendData(){
    
    sessionId = getSessionId();
    var ehrid = $("#ehrid").val();
    var teza = $("#teza").val();
    var visina = $("#visina").val();
    var datumvnosa = $("#datumVnosa").val();
    
    if(!ehrid || !teza || !visina || ehrid.trim().length == 0 ||
      teza.trim().length == 0 || visina.trim().length == 0){
           $("#dataMessage").html("<h1><span class='label " +
            "label-warning fade-in lb-lg'>Preverite vnesene podatke!</span></h1>");
      }else{
          $.ajaxSetup({
             headers: {"Ehr-Session": sessionId} 
          });
          
          var podatki = {
               "ctx/language": "en",
               "ctx/territory": "SI",
               "ctx/time": datumvnosa,
               "vital_signs/height_length/any_event/body_height_length": visina,
               "vital_signs/body_weight/any_event/body_weight": teza
       };
         
         var parametriZahteve = {
           ehrId: ehrid,
           templateId: 'Vital Signs',
           format: 'FLAT'
         };
         
         $.ajax({
            url: baseUrl + "/composition?" + $.param(parametriZahteve),
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(podatki), 
            
            success: function (res) {
                $("#dataMessage").html(
                  "<h3><span class='label label-success fade-in lb-lg'>" +
                  res.meta.href + "</span></h3>");
                },
            error: function(err) {
                $("#dataMessage").html(
                "<h1><span class='label label-danger fade-in lb-lg'>Error: '" +
                JSON.parse(err.responseText).userMessage + "'!</h1>");
            }
             
         });
          
      }
    
}

function addRowHandlers(x,res) {
 
    var table = document.getElementById("tableId");
    var rows = table.getElementsByTagName("tr");
    for (var i = 1; i < rows.length; i++) {
        var row = table.rows[i];
        row.onclick = function(){
                if(rows.length>=6){
                   
                $("#graf").modal();
                $(".prostor").empty();
                $(".prostor").append("<div id='stolpicni-diagram'></div>");
                
                setTimeout( function(){
                    drawGraph(x,res);
                    
                },500);

                
                }else{
                    alert("Vnesi več podatkov (vsaj 6)");
                }
              
                var cell = this.getElementsByTagName("td")[0];
                var id = cell.innerHTML;
                var cell1 = this.getElementsByTagName("td")[1];
                var id1 = cell1.innerHTML;
                var cell2 = this.getElementsByTagName("td")[2];
                var id2 = cell2.innerHTML;
                var output = Math.round(calculateBMI(id1,id2));
                
                if (output<18.5){
                    $("#naslov").text("Podhranjenost BMI = "+output);
                }
                if (output>=18.5 && output<=25){
                    $("#naslov").text("Normalna teža BMI =  "+output);
                 }
                 if (output>=25 && output<=30){
                    $("#naslov").text("Normalna teža BMI  = "+output);
                 }
                if (output>=31 && output <=40){
                    $("#naslov").text("Debelost 1. stopnje BMI = "+output);
                }
                if(output>=41){
                  $("#naslov").text("Debelost 2. stopnje BMI = "+output);
                }

            }
          };
         
    }

function calculateBMI(weight, height){
    return weight/(height/100*height/100);
}

function drawGraph(x,res){
    
    var len = x.length;
    var chart = Morris.Bar({
      element: 'stolpicni-diagram',
            data: [
                { y: x[len-6][1], a: x[len-6][0],  b: res[len-6].height},
                { y: x[len-5][1], a: x[len-5][0],  b: res[len-5].height},
                { y: x[len-4][1], a: x[len-4][0],  b: res[len-4].height},
                { y: x[len-3][1], a: x[len-3][0],  b: res[len-3].height},
                { y: x[len-2][1], a: x[len-2][0],  b: res[len-2].height},
                { y: x[len-1][1], a: x[len-1][0],  b: res[len-1].height}
        ],
          xkey: 'y',
          ykeys: ['a','b'],
          xLabels: "day",
          labels: ['Teza','Visina'],
          resize:true,
          axes:false
    });

}


var c = 0;
function recData(){
   
   c++;
   console.log("c: "+c);
   sessionId = getSessionId();
   
   var ehrId = $("#ehrid").val();
   
   if(!ehrId || ehrId.trim().length == 0){
        $("#dataMessage").html("<h1><span class='label " +
        "label-warning fade-in lb-lg'>Preverite ehrId!</span></h1>");
   }else if(c%2==0){
       $(".smt").empty();
       $(".smt").append("<div id='showInfo'></div>");
    }else{
          
      var x = false;
    
       $.ajax({
          url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
          type: 'GET',
          headers: {"Ehr-Session": sessionId},
       
          success: function(data){
              
             $("#showInfo").html("");
              var party = data.party;
              $("#showInfo").html("<h1><br/><span>Pridobivanje " +
                "podatkov za <b></b> bolnika <b>'" + party.firstNames +
                " " + party.lastNames + "'</b>.</span><br/><br/></h1>");
            
             $.ajax({
                url: baseUrl + "/view/" + ehrId + "/" + "weight",
                type: 'GET',
                headers: {"Ehr-Session": sessionId}, 
                success: function(res){
                  if(res.length>0){
                     x = new Array(res.length);
                     for (var i = 0; i < res.length; i++) {
                        x[i] = new Array(2);
                     }
                    
                     for (var i in res) {
                         x[i][0]=res[i].weight;
                         x[i][1]=res[i].time.substring(0,10);
                    }
                     
                  }else{
                      $("#showInfo").html("<h1><span class='label label-warning fade-in'>" +"NO INFO!</span></h1>");
                  }  
                },
                error: function(err) {
                     $("#izpisi").html(
                      "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                      JSON.parse(err.responseText).userMessage + "'!");
                    }
            });
            
            $.ajax({
                url: baseUrl + "/view/" + ehrId + "/" + "height",
                type: 'GET',
                headers: {"Ehr-Session": sessionId}, 
                success: function(res){
                   
                  if(res.length>0){
                      var results = "<table class='table table-striped table-hover' id='tableId'><thead>"+"<tr align='center' style='color:white;'><td>Datum</td>"+"<td>Teza</td>"+"<td>Visina</td>";
                     for (var i in res) {
                         if(x[i][1] && x[i][0] && res[i].height){
                                results += "<tbody><tr align='center'>"+"<td>"+x[i][1]+"</td>"
                                          +"<td>"+x[i][0]+"</td>"+"<td>"+res[i].height+"</td>"+"</tbody>";
                                 }
                            }
                        
                          $("#showInfo").append(results);
                          addRowHandlers(x,res);
                         
                  }else{
                      $("#showInfo").html("<h1><span class='label label-warning fade-in'>" +"NO INFO!</span></h1>");
                  }  
                },
                error: function(err) {
                     $("#izpisi").html(
                      "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                      JSON.parse(err.responseText).userMessage + "'!");
                    }
            });
         
          },
            
            error: function(err) {
                console.log(err);
                $("#showInfo").html(
                "<h1><span class='label label-danger fade-in'>Napaka '" +
                JSON.parse(err.responseText).userMessage + "'!</h1>");
            }
       });
   }
}

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        alert("Geolocation is not supported by this browser.");
    }
}
function showPosition(position) {
    var lat = position.coords.latitude;
    var lon = position.coords.longitude;
    var URL = "https://api.darksky.net/forecast/af0b84ab9a3d0b8837438bd8de829b35/"+lat+","+lon+"?units=si"
       $.ajax({
          url: URL,
          dataType : "jsonp",
          
          success: function(res){
              var t = res.currently.apparentTemperature;
              var s = res.hourly.summary;
              var p = res.currently.pressure;
              var h = Math.round(res.currently.humidity*100);
              console.log(res);
              $("#porocilo").append("<h1>Summary: "+s+"</h1>");
              $("#porocilo").append("<br><h1>Temperature: "+t+"&deg;C"+"</h1></br>");
              $("#porocilo").append("<br><h1>Pressure: "+p+"mb"+"</h1></br>");
              $("#porocilo").append("<br><h1>Humidity: "+h+"%"+"</h1></br>");
             
             console.log(res);
              
          }
       });
    
}

$(document).ready(function(){
    //mainGenerator();
    getLocation();
    console.log("haha");
});